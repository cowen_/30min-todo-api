import web
import datetime as dt

urls = (
	'/lists/(\d*)', 'TodoListAPI', # listID
	'/items/(\d*)', 'TodoItemAPI', # itemID
	'/date/(\d+)/(\d\d\d\d-\d\d-\d\d)', 'ListItemsAPI' # listID / Date (YYYY-MM-DD)
)

todoLists = {} # { listID : [todoItems] }
todoLists['itemId'] = 0 # Hack: web.py test server doesn't persist global ints
todoLists['listId'] = 0 # Hack: web.py test server doesn't persist global ints

class TodoItem:
	def __init__(self, id, task):
		self.id = id
		self.task = task
		self.created = dt.datetime.now()

	def __str__(self):
		return str((self.id, self.task, str(self.created)))


"""
Quick method to print off an iterable object of todoItems.
"""
def printList(items):
	return '\n'.join(str(item) for item in items) or 'Sorry, there are no items in this list'


"""
Search all lists for an item with the given ID
and return a tuple of its parent list ID, and the
item itself. 
"""
def findItem(itemId):
	for listId in todoLists:
		if isinstance(listId, int): 
			items = todoLists[listId]
			for item in items:
				if item.id == itemId:
					return (listId, item)
	return None

class TodoListAPI:
	def GET(self, listId): 
		print todoLists
		if listId == '':
			web.notfound()

		try:
			id = int(listId)
			t_list = todoLists[id]
			return printList(t_list)
		except:
			web.notfound()

	def POST(self, _):
		listId = todoLists['listId']
		todoLists[listId] = []
		todoLists['listId'] = listId + 1

	def DELETE(self, listId): 
		if listId == '':
			web.notfound()

		try:
			id = int(listId)
			del(todoLists[id])
		except:
			web.notfound()

class TodoItemAPI:
	def GET(self, itemId): 
		id = int(itemId) or None
		(listId, item) = findItem(id)
		return item or web.notfound()

	def POST(self, _):
		(listId, text) = str(web.data()).split('\n')
		listId = int(listId)

		itemId = todoLists['itemId']
		item = TodoItem(itemId, text)
		try:
			todoLists[listId].append(item)
			todoLists['itemId'] = itemId + 1
		except:
			web.notfound()

	def DELETE(self, itemId): 
		id = int(itemId)
		(listId, item) = findItem(id)
		if item is not None:
			todoLists[listId].remove(item)
		else:
			web.notfound()

	def PUT(self, itemId): 
		newTask = web.data()
		id = int(itemId)
		(listId, item) = findItem(id)

		if item is not None:
			item.task = newTask
		else:
			web.notfound()

class ListItemsAPI:
	def GET(self, listID, dateStr): 
		try:
			id = int(listID)
			date = dt.datetime.strptime(dateStr, '%Y-%m-%d')
			t_list = todoLists[id]
			return printList(item for item in t_list if item.created >= date)
		except:
			web.notfound()

if __name__ == '__main__':
	app = web.application(urls, globals())
	app.run()
